import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File

fun readCSVFile(file: File, bucketTimings: Array<ULong>): Pair<Map<ULong, MutableList<VegetaEntry>>, MutableList<VegetaEntry>> = csvReader().open(file) {
    val buckets = bucketTimings.map { Pair<ULong, MutableList<VegetaEntry>>(it, mutableListOf()) }.sortedByDescending { it.first }
    val errorList = mutableListOf<VegetaEntry>()
    var stringified = readNext()
    while (stringified != null) {
        val entry = VegetaEntry(stringified)
        if (entry.error.isNotEmpty())
            errorList.add(entry)
        else buckets.first { it.first < entry.requestLatency }.second.add(entry)
        stringified = readNext()
    }

    return@open Pair(buckets.toMap(), errorList)
}

fun main(args: Array<String>) {
    val csvDirectory = File(args[0])
    val csvFiles = csvDirectory.listFiles { file, s -> s.endsWith(".csv") }.sortedBy {
        it.nameWithoutExtension.split("-").last().toLong()
    }

    val buckets = arrayOf<ULong>(0U, 300_000U, 400_000U, 500_000U, 600_000U, 700_000U, 800_000U, 900_000U, 1_000_000U, 1_100_000U, 1_200_000U, 1_300_000U, 1_400_000U, 2_000_000U, ULong.MAX_VALUE)
    //val yAxisLabels = arrayOf("100 RPS", "200 RPS", "300 RPS")
    val yAxisLabels = csvFiles.map { it.nameWithoutExtension.split("-").last() + " RPS"}.toTypedArray()


    val heatMap = Heatmap(buckets, yAxisLabels)

    //read a file
    csvFiles.forEachIndexed { columnIndex, file ->
        val read = readCSVFile(file, buckets)
        if (read.second.count() > 0)
            heatMap.AddErrorsToColumn(columnIndex, read.second)
        heatMap.AddDataToColumn(columnIndex, read.first)
    }
    println(heatMap.RenderCLI())
    val latexTable = heatMap.RenderLatex()
    println(latexTable)
}

private fun String.toFile(): File = File(this)