import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File

typealias NanoSeconds = ULong
typealias HTTPStatusCode = UShort

class VegetaEntry(stringified: List<String>) {
    //Parse data
    val unixTimestamp: NanoSeconds = stringified[0].toULong()
    val httpStatusCode: HTTPStatusCode = stringified[1].toUShort()
    val requestLatency: NanoSeconds = stringified[2].toULong()
    val bytesOut: ULong = stringified[3].toULong()
    val bytesIn: ULong = stringified[4].toULong()
    val error: String = stringified[5]
    val encodedResponseBody: String = stringified[6]
    val attackName: String = stringified[7]
    val requestSequenceNumber: ULong = stringified[8].toULong()
    val httpMethod: String = stringified[9]
    val targetURL: String = stringified[10]
    val encodedResponseHeaders: String = stringified[11]
}
