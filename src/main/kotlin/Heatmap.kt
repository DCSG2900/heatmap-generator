import java.math.BigDecimal
import java.math.RoundingMode

data class ResponseBucket(
    val upperResponseBound: ULong,
    val columns: Array<MutableList<VegetaEntry>>
) {
    fun RenderCLI(longestDiget: Int): String {
        var result = "${upperResponseBound.toString().padStart(longestDiget)} |"
        columns.forEach {
            result += "\t[${it.count().toString().padStart(6)}]"
        }
        return result
    }

    fun ULong.ToFormalString(): String = when {
        this == ULong.MAX_VALUE -> "+INF"
        this >= 1_000_000_000U -> "${(this.toFloat() / 1_000_000_000).LimitDecimals(1)} \$s\$"
        this >= 1_000_000U -> "${(this.toFloat() / 1_000_000).LimitDecimals(1)} \$ms\$"
        this >= 1_000U -> "${this / 1_000U} $\\mu s$"
        this == 0UL -> "MIN"
        else -> "${this} \$ns\$"
    }.padStart(5)

    fun Color(pres: Float): Pair<String, String> {
        val blackOnWhite = pres < 50
        val fullness = 100.0f - pres
        val fgColor = if (blackOnWhite) {
            "white"
        } else "black"

        return Pair("black!$fullness", "$fgColor!100")
        /*
        if (blackOnWhite) {
            return Pair("black!$fullness", "white!$pres")
        } else
        return Pair("white!$pres", "black!$fullness")
        */
    }

    fun RenderLatex(longestDiget: Int, columnTotal: Array<ULong>): String {
        val stringifiedResponseBound = upperResponseBound.ToFormalString()
        var result = "\\hline\n\\textbf{$stringifiedResponseBound} "
        columns.forEachIndexed { index, column ->
            var precentage = ((100.0F * column.count()) / columnTotal[index].toFloat())
            if (!precentage.isNaN() && !precentage.isInfinite() && precentage.isFinite()) {
                precentage = (precentage.toBigDecimal() - (precentage.toBigDecimal().rem(0.1f.toBigDecimal()))).toFloat()
                val (bgColor,fgColor) = Color(precentage)
                val total = "${column.count()} ($precentage\\%)"
                result += "& \\cellcolor{$bgColor}{\\color{$fgColor}$total} "
            } else {
                result += "& ERROR "
            }
        }
        return result + "\\\\"
    }
}

fun Float.LimitDecimals(numDecimals: Int): BigDecimal = toBigDecimal().setScale(numDecimals, RoundingMode.HALF_EVEN)
class Heatmap(buckets: Array<ULong>, val yAxisLabels: Array<String>) {
    val yAxisCount = yAxisLabels.size
    val buckets = buckets.map { ResponseBucket(it, Array(size = yAxisCount, init = { index -> mutableListOf() })) }
    val errors = Array<MutableList<VegetaEntry>>(yAxisCount, init = { index -> mutableListOf()})
    val totalPerColumn = Array<ULong>(yAxisCount, init = { 0UL })

    fun AddErrorsToColumn(columnIndex: Int, second: MutableList<VegetaEntry>) = errors[columnIndex].addAll(second)
    fun AddDataToColumn(columnIndex: Int, buckitedData: Map<ULong, MutableList<VegetaEntry>>) {
        buckitedData.forEach { bucketBound, data ->
            val bucket = buckets.firstOrNull { it.upperResponseBound == bucketBound }?: error("Failed to find bucket in heatmap with timing $bucketBound")
            bucket.columns[columnIndex].addAll(data)
            totalPerColumn[columnIndex] += data.size.toULong()
        }
    }

    fun PrintErrors(): String = "Errors found!\n"

    fun RenderCLI(): String {
        var result = ""
        val hasErrors = errors.firstOrNull { it.count() > 0} != null
        if (hasErrors)
            result += PrintErrors()

        val longestDiget = buckets.maxByOrNull { it.upperResponseBound.toString().length }?.upperResponseBound.toString().length
        result += RenderYAxisCLI(longestDiget)


        buckets.sortedByDescending { it.upperResponseBound }.forEach {
            result += it.RenderCLI(longestDiget) + "\n"
        }
        return result
    }

    private fun RenderYAxisCLI(longestDiget: Int): String {
        var result = ""
        result += "-".repeat(longestDiget + 2)
        for (column in 0 until yAxisCount)
            result += totalPerColumn[column].toString().padCenter(11, '-')
        //Heading padding
        result += "-".repeat(longestDiget + 2)
        for (yAxis in 0 until yAxisCount) {
            result += yAxisLabels[yAxis].padCenter(11, '-')
        }
        result += "\n"
        return result
    }

    fun RenderLatex(colType: Char = 'l'): String {
        var result = "\\begin{tabular}{" + "$colType|".repeat(yAxisCount+1) + "}\n"
        val longestDiget = buckets.maxByOrNull { it.upperResponseBound.toString().length }?.upperResponseBound.toString().length
        //result += RenderYAxisCLI(longestDiget)

        buckets.sortedByDescending { it.upperResponseBound }.forEach {
            result += it.RenderLatex(longestDiget, totalPerColumn) + "\n"
        }
        result += "\\cline{1-$yAxisCount}"
        for (i in 0 until yAxisCount) {
            result += "  & \\textbf{${yAxisLabels[i]}}"
        }
        result += "\\\\\n\\end{tabular}"
        return result
    }
}

private fun String.padCenter(i: Int, padding: Char): String = (this as CharSequence).padCenter(i, padding).toString()

private fun CharSequence.padCenter(i: Int, padding: Char): CharSequence {
    val startPad = (i - length) / 2
    val sb = StringBuilder(i)
    for (i in 0..startPad)
        sb.append(padding)
    sb.append(this)
    for (i in 1..(i - (startPad + length)))
        sb.append(padding)

    return sb
}
